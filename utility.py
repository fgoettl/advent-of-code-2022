def load_input(day):
    with open("input_" + str(day), "r") as f:
        return [x.replace("\n", "") for x in f]


def longest_common_subsequence(L):
    n = len(L)

    class n_dim_array:
        def __init__(self, L):
            self.N = [len(x) + 1 for x in L]
            self.n = prod(self.N)
            self.data = [0] * self.n

        def __getitem__(self, item):
            return self.data[self.index(item)]

        def __setitem__(self, key, value):
            self.data[self.index(key)] = value

        def index(self, indices):
            k = 1
            res = 0
            for n_i, index in zip(self.N, indices):
                res += k * index
                k *= n_i
            return res

    def gen_tupels(L):
        N = [len(x) + 1 for x in L]
        t = [0] * len(L)
        stop = False
        while not stop:
            yield [x for x in t]
            t[0] = t[0] + 1
            for i in range(len(N)):
                if t[i] >= N[i]:
                    t[i] = 0
                    if i + 1 < len(N):
                        t[i + 1] = t[i + 1] + 1
                    else:
                        stop = True
                        break
                else:
                    break

    def lower_tupels(T):
        res = []
        for i in range(len(T)):
            x = [t for t in T]
            x[i] = x[i] - 1
            res.append(x)
        return res

    def m_i(T, i):
        x = [t for t in T]
        x[i] = x[i] - 1
        return x

    x = n_dim_array(L)
    for T in gen_tupels(L):
        if any([i == 0 for i in T]):
            x[T] = 0
        elif all_equal([l[t - 1] for l, t in zip(L, T)]):
            x[T] = x[[t - 1 for t in T]] + 1
        else:
            x[T] = max([x[k] for k in lower_tupels(T)])

    lcs_length = x.data[-1]
    T = [len(x) for x in L]

    def backtrack_to_corner(x, T):
        value = x[T]
        for i in range(len(T)):
            T_ = m_i(T, i)
            while x[T_] == value:
                T = T_
                T_ = m_i(T, i)
        return T

    lcs = []
    for _ in range(lcs_length):
        T = backtrack_to_corner(x, T)
        lcs.insert(0, L[0][T[0] - 1])
        T = [t - 1 for t in T]
    return lcs


def all_equal(l):
    if not l:
        return True
    return all([l[0] == x for x in l])


def partition_list(l, n):
    # Generator. Returns n Elements of the list l at a time. If len(l) is no integer, excess elements are discarded.
    le = len(l)
    i = 0
    while i + (n - 1) < le:
        yield l[i:i + n]
        i = i + n


def consecutive_pairs(l):
    le = len(l)
    i = 0
    while i + 1 < le:
        yield l[i], l[i + 1]
        i += 1


def multisplit(s, *separators):
    strings = [s]
    for sep in separators:
        strings = flatten1([s.split(sep) for s in strings])
    return [s for s in strings if s != ""]


def rank(l, x):
    # Index of the last Element in l that is <= x; l has to be a sorted list
    low = -1
    high = len(l)
    while high > low + 1:
        mid = int((low + high) / 2)
        if x >= l[mid]:
            low = mid
        else:
            high = mid
    return low


def flatten1(l):
    # Flattens an iterable of iterables by one level, e.g. [[1,2],(3,4),[[5,6]]] -> [1,2,3,4,[5,6]]
    return [x for X in l for x in X]


def prod(l):
    r = 1
    for i in l:
        r *= i
    return r


def on_list(f):
    def f_(l):
        return [f(x) for x in l]

    return f_


class Stack:
    def __init__(self):
        self.content = []

    def empty(self):
        return not self.content

    def push(self, x):
        self.content.append(x)

    def pop(self):
        if not self.empty():
            return self.content.pop(-1)
        else:
            return None

    def peek(self):
        if not self.empty():
            return self.content[-1]
        else:
            return None

    def popk(self, k):  # pops the top most k elements, bottom-up, and returns them as a list.
        if len(self.content) < k:
            return None
        else:
            return reversed([self.pop() for _ in range(k)])


class Queue:
    _content: list
    _current_head: int

    def __init__(self):
        self._content = []
        self._current_head = 0

    def enqueue(self, item):
        self._content.append(item)

    def empty(self):
        return self._current_head >= len(self._content)

    def dequeue(self):
        if self.empty():
            return None
        else:
            x = self._content[self._current_head]
            self._current_head += 1
            if self._current_head >= len(self._content) / 2 + 10:
                self._content = self._content[self._current_head:]
                self._current_head = 0
            return x
