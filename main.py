from utility import load_input, on_list, partition_list, flatten1, Stack, prod, Queue, consecutive_pairs, rank, \
    multisplit
import math
from random import randint


def day1(part):
    ls = load_input(1)
    calories = []
    r = 0
    for l in ls:
        if l == "":
            calories.append(r)
            r = 0
        else:
            r += int(l)
    calories.append(r)
    return max(calories) if part == 1 else sum(sorted(calories)[-3:])


def day2(part):
    ls = load_input(2)
    instructions = [line.split(" ") for line in ls]
    # We represent Rock, Paper, Scissors by 0, 1, 2, respectively.
    translation = {"A": 0, "B": 1, "C": 2, "X": 0, "Y": 1, "Z": 2}

    def number_representation(instructions):  # For part 1
        return [(translation[i1], translation[i2]) for (i1, i2) in instructions]

    def calculate_matches(instructions):  # For part 2

        def response(op, instr):
            return (translation[op] + (-1 if instr == "X" else 0 if instr == "Y" else 1)) % 3

        return [(translation[i1], response(i1, i2)) for (i1, i2) in instructions]

    def score(op, my):
        outcome = ((my - op + 1) % 3) - 1  # -1 if we lose, 0 if it's a tie, 1 if we win
        return (3 + 3 * outcome) + (my + 1)

    def day2_1():
        return sum([score(*match) for match in number_representation(instructions)])

    def day2_2():
        return sum([score(*match) for match in calculate_matches(instructions)])

    return day2_1() if part == 1 else day2_2()


def day3(part):
    @on_list
    def split_halves(s):
        h = int(len(s) / 2)
        return s[:h], s[h:]

    @on_list
    def common_char(strings):
        for c in strings[0]:
            if all(c in s for s in strings[1:]):
                return c

    @on_list
    def prio(c):
        if "a" <= c <= "z":
            return ord(c) - ord("a") + 1
        else:
            return ord(c) - ord("A") + 27

    ls = load_input(3)
    if part == 1:
        return sum(prio(common_char(split_halves(ls))))
    else:
        return sum(prio(common_char(partition_list(ls, 3))))


def day4(part):
    ls = load_input(4)
    section_pairs = [[int(x) for x in l.replace("-", ",").split(",")] for l in ls]

    def contains(section_pair):
        a, b, c, d = section_pair
        return a <= c <= d <= b or c <= a <= b <= d

    def overlaps(section_pair):
        a, b, c, d = section_pair
        return a <= c <= b or a <= d <= b or c <= a <= d or c <= b <= d

    if part == 1:
        return sum(contains(x) for x in section_pairs)
    else:
        return sum(overlaps(x) for x in section_pairs)


def day5(part):
    ls = load_input(5)

    def parse_input(ls):
        # Returns (s,i), where s is a list of stacks (datastructure), each containing the crates of one stack (puzzle),
        # and i is the sequence of instructions, each "move x from y to z" represented as the triple (x,y,z).
        stack_lines = []
        instr_lines = []
        num_stacks = 0
        for l in ls:
            if len(l) == 0:
                pass
            elif l[0] == "m":
                instr_lines.append(l)
            elif l[1] == "1":
                num_stacks = int((len(l) + 1) / 4)
            else:
                stack_lines.append(l)
        instructions = [[int(x) for x in l.replace("move", "").replace("from", "").replace("to", "").split()] for l in
                        instr_lines]
        stacks = [Stack() for _ in range(num_stacks)]
        for l in reversed(stack_lines):
            l = l + " "
            current_stack = 0
            for x in partition_list(l, 4):
                if x[0] == "[":
                    stacks[current_stack].push(x[1])
                current_stack += 1
        return stacks, instructions

    stacks, instructions = parse_input(ls)

    def process_instr_9000(instr, stacks):
        x, y, z, = instr
        for _ in range(x):
            stacks[z - 1].push(stacks[y - 1].pop())

    def process_instr_9001(instr, stacks):
        x, y, z = instr
        for crate in stacks[y - 1].popk(x):
            stacks[z - 1].push(crate)

    process_instr = process_instr_9000 if part == 1 else process_instr_9001
    for instr in instructions:
        process_instr(instr, stacks)
    return "".join([s.peek() for s in stacks])


def day6(part):
    input_word = load_input(6)[0]

    def consecutive(l, n):
        i = 0
        while i + (n - 1) < len(l):
            yield [l[j] for j in range(i, i + n)]
            i += 1

    def all_different(l):
        for i in range(len(l)):
            for j in range(i + 1, len(l)):
                if l[i] == l[j]:
                    return False
        return True

    def first_n_different(input_word, n):
        pos = n - 1
        for l in consecutive(input_word, n):
            pos += 1
            if all_different(l):
                return pos

    return first_n_different(input_word, 4) if part == 1 else first_n_different(input_word, 14)


def day7(part):
    class file_tree():
        def __init__(self, dir, name, parent=None, file_size=0):
            self.dir = dir
            self.name = name
            self.children = []
            self.file_size = file_size
            self.parent = parent
            self.total_size = 0

        def add_child(self, dir, name, file_size=0):
            for child in self.children:
                if child.dir == dir and child.name == name:
                    return
            self.children.append(file_tree(dir, name, self, file_size))

        def sub_dir(self, name):
            for child in self.children:
                if child.dir and child.name == name:
                    return child
            self.add_child(True, name, self)
            return self.sub_dir(name)

        def print(self, depth=0):
            if self.dir:
                print("  " * depth + "- " + self.name + " (dir) ")
                for child in self.children:
                    child.print(depth + 1)
            else:
                print("  " * depth + "- " + self.name + " (file, size=" + str(self.file_size) + ")")

        def calculate_total_sizes(self):
            for child in self.children:
                child.calculate_total_sizes()
            self.total_size = self.file_size + sum(c.total_size for c in self.children)

        def accumulate(self, f,
                       comb=sum):  # Applies f to the current dir/file and all dirs/files in it and combines them via comb.
            return comb([f(self)] + [c.accumulate(f, comb) for c in self.children])

    # Build up file tree
    command_lines = load_input(7)
    root = file_tree(True, r"/")
    current_dir = None
    for c_line in command_lines:
        if c_line[0] == "$":
            if c_line[2] == "c":  # a cd command
                target_dir = c_line[5:]
                if target_dir == r"/":
                    current_dir = root
                elif target_dir == "..":
                    current_dir = current_dir.parent
                else:
                    current_dir = current_dir.sub_dir(target_dir)
            else:  # a ls command; nothing to do on this line
                pass
        else:  # result of a ls command
            if c_line[0] == "d":  # a directory
                current_dir.add_child(True, c_line[4:])
            else:  # a file
                size_s, name = c_line.split(" ")
                size = int(size_s)
                current_dir.add_child(False, name, size)
    root.calculate_total_sizes()

    def size_if_above_100000(node):
        if node.dir and node.total_size <= 100000:
            return node.total_size
        return 0

    needed_space = root.total_size - 40000000

    def size_if_above_needed(node):
        if node.dir and node.total_size >= needed_space:
            return node.total_size
        return 70000001

    if part == 1:
        return root.accumulate(size_if_above_100000)
    else:
        return root.accumulate(size_if_above_needed, min)


def day8(part):
    ls = load_input(8)
    input = [[int(x) for x in l] for l in ls]
    rows = len(input)
    cols = len(input[0])
    visible = [False for _ in range(rows * cols)]

    def tree(row, col):
        return input[row][col]

    def mark_visible(row, col):
        visible[row * cols + col] = True

    # mark trees visible from the left:
    for row in range(rows):
        highest = -1
        for col in range(cols):
            if tree(row, col) > highest:
                mark_visible(row, col)
                highest = tree(row, col)
    # mark trees visible from the right:
    for row in range(rows):
        highest = -1
        for col in reversed(list(range(cols))):
            if tree(row, col) > highest:
                mark_visible(row, col)
                highest = tree(row, col)
    # mark trees visible from the top:
    for col in range(cols):
        highest = -1
        for row in range(rows):
            if tree(row, col) > highest:
                mark_visible(row, col)
                highest = tree(row, col)
    # mark trees visible from the bottom:
    for col in range(cols):
        highest = -1
        for row in reversed(list(range(rows))):
            if tree(row, col) > highest:
                mark_visible(row, col)
                highest = tree(row, col)
    if part == 1:
        return sum(visible)

    def view_distance(start, direction):
        distance = 0
        current_row, current_col = start
        dir_r, dir_c = direction
        height = tree(current_row, current_col)
        while True:
            current_row += dir_r
            current_col += dir_c
            if not (0 <= current_row < rows and 0 <= current_col < cols):  # edge
                return distance
            elif tree(current_row, current_col) >= height:  # tree of >= height
                return distance + 1
            else:  # tree of smaller height
                distance += 1

    def product(l):
        r = 1
        for x in l:
            r *= x
        return r

    def scenic_score(row, col):
        return product(view_distance((row, col), direction) for direction in [(1, 0), (-1, 0), (0, 1), (0, -1)])

    if part == 2:
        return max([scenic_score(row, col) for row in range(rows) for col in range(cols)])


def day9(part):
    ls = load_input(9)
    splits = [l.split(" ") for l in ls]
    instructions = [(a, int(b)) for a, b in splits]

    def calculate_area_and_start_pos(instructions):
        x = xmin = xmax = y = ymax = ymin = 0
        for dir, amount in instructions:
            if dir == "U":
                y += amount
                if y > ymax:
                    ymax = y
            elif dir == "D":
                y -= amount
                if y < ymin:
                    ymin = y
            elif dir == "R":
                x += amount
                if x > xmax:
                    xmax = x
            else:
                x -= amount
                if x < xmin:
                    xmin = x
        return xmax - xmin + 1, ymax - ymin + 1, -xmin, -ymin

    def mark(visited, t_pos):
        visited[t_pos[0] + xsize * t_pos[1]] = True

    def step_head(dir, h_pos):
        if dir == "U":
            h_pos[1] += 1
        elif dir == "D":
            h_pos[1] -= 1
        elif dir == "R":
            h_pos[0] += 1
        else:
            h_pos[0] -= 1

    def tension(lead_knot, follow_knot):
        lkx, lky = lead_knot
        fkx, fky = follow_knot
        rr = lkx > fkx + 1
        ll = lkx < fkx - 1
        uu = lky > fky + 1
        dd = lky < fky - 1
        r = 1 if lkx > fkx else 0
        l = 1 if lkx < fkx else 0
        u = 1 if lky > fky else 0
        d = 1 if lky < fky else 0
        gets_dragged = rr or ll or uu or dd
        return gets_dragged, r, l, u, d

    def drag(lead_knot, follow_knot):
        gets_dragged, r, l, u, d = tension(lead_knot, follow_knot)
        if gets_dragged:
            follow_knot[0] += r - l
            follow_knot[1] += u - d

    def carry_out_step(dir, knot_positions):
        step_head(dir, knot_positions[0])
        for i in range(len(knot_positions) - 1):
            drag(knot_positions[i], knot_positions[i + 1])

    xsize, ysize, x, y = calculate_area_and_start_pos(instructions)
    visited = [False] * (xsize * ysize)
    knot_positions = []
    for _ in range(2 if part == 1 else 10):
        knot_positions.append([x, y])
    mark(visited, knot_positions[-1])
    for dir, amount in instructions:
        for _ in range(amount):
            carry_out_step(dir, knot_positions)
            mark(visited, knot_positions[-1])
    return sum(visited)


def day10(part):
    ls = load_input(10)
    instructions = []
    NOOP = 0
    ADDX = 1
    for l in ls:
        parts = l.split(" ")
        if parts[0] == "noop":
            instructions.append((NOOP, 0))
        else:
            instructions.append((ADDX, int(parts[1])))

    class CPU:
        _next_cycle: int
        _X: int
        signal_strength_sum: int
        _pixels: list[list[bool]]
        _rows: int
        _cols: int

        def __init__(self, rows=6, cols=40):
            self._X = 1
            self._next_cycle = 1
            self.signal_strength_sum = 0
            self._pixels = []
            for _ in range(rows):
                self._pixels.append([])
            self._rows = rows
            self._cols = cols

        def carry_out_instruction(self, instr):
            t, value = instr
            if t == NOOP:
                self._do_cycle()
            elif t == ADDX:
                for _ in range(2):
                    self._do_cycle()
                self._X += value

        def _do_cycle(self):
            self._track_signal_strength()
            self._draw_pixel()
            self._next_cycle += 1

        def _track_signal_strength(self):
            if self._next_cycle % 40 == 20 and self._next_cycle <= 220:
                self.signal_strength_sum += self._X * self._next_cycle

        def _draw_pixel(self):
            p_row = int((self._next_cycle - 1) / self._cols)
            p_col = (self._next_cycle - 1) % self._cols
            if p_row >= self._rows:
                return
            if self._X - 1 <= p_col <= self._X + 1:
                self._pixels[p_row].append(True)
            else:
                self._pixels[p_row].append(False)

        def get_image(self):
            def symb(x):
                return "##" if x else "__"

            s = ""
            for row in self._pixels:
                line = "".join([symb(x) for x in row])
                s = s + line + "\n"
            return s

    cpu = CPU()
    for instr in instructions:
        cpu.carry_out_instruction(instr)
    if part == 1:
        return cpu.signal_strength_sum
    else:
        return cpu.get_image()


def day11(part):
    ls = load_input(11) + [""]

    class Monkey:
        def __init__(self, L):
            self.items = [int(x) for x in L[1].split("Starting items: ")[1].split(", ")]
            a, b, c = L[2].split("Operation: new = ")[1].split(" ")
            self.operation = lambda x: (lambda a, c: a * c if b == "*" else a + c)(x if a == "old" else int(a),
                                                                                   (x if c == "old" else int(c)))
            self.div_num = int(L[3].split("by ")[1])
            t = lambda x: (x % self.div_num == 0)
            self.target = lambda x: int(L[4].split("monkey ")[1]) if t(x) else int(L[5].split("monkey ")[1])
            self.toss_count = 0

        def process(self, monkey_list):
            while self.items:
                i = self.items.pop()
                i = int(self.operation(i) / 3)
                monkey_list[self.target(i)].items.append(i)
                self.toss_count += 1

        def process_modulo(self, monkey_list, modulus):
            while self.items:
                i = self.items.pop()
                i = self.operation(i) % modulus
                monkey_list[self.target(i)].items.append(i)
                self.toss_count += 1

    monkeys = [Monkey(L) for L in partition_list(ls, 7)]
    if part == 1:
        for _ in range(20):
            for monk in monkeys:
                monk.process(monkeys)
    else:
        safe_modulus = prod([monk.div_num for monk in monkeys])
        for _ in range(10000):
            for monk in monkeys:
                monk.process_modulo(monkeys, safe_modulus)
    return prod(sorted([m.toss_count for m in monkeys])[-2:])


def day12(part):
    ls = load_input(12)
    xsize = len(ls[0])
    ysize = len(ls)

    def height(c):
        if c == "S":
            return 0
        elif c == "E":
            return 25
        else:
            return ord(c) - ord("a")

    heights = [height(c) for line in ls for c in line]

    def find_S_and_E(ls):
        x = 0
        y = 0
        s_pos = (0, 0)
        e_pos = (0, 0)
        for l in ls:
            for c in l:
                if c == "S":
                    s_pos = (x, y)
                elif c == "E":
                    e_pos = (x, y)
                x += 1
            y += 1
            x = 0
        return s_pos, e_pos

    s, e = find_S_and_E(ls)

    class HeightGraph:
        _xsize: int
        _ysize: int
        _heights: list[int]

        def __init__(self, heights, xsize, ysize):
            self._xsize = xsize
            self._ysize = ysize
            self._heights = heights

        def _orth_neighbors(self, p: (int, int)):
            x, y = p
            result = []
            if x > 0:
                result.append((x - 1, y))
            if x < self._xsize - 1:
                result.append((x + 1, y))
            if y > 0:
                result.append((x, y - 1))
            if y < self._ysize - 1:
                result.append((x, y + 1))
            return result

        def index_of_point(self, p: (int, int)):
            return p[0] + self._xsize * p[1]

        def height_of_point(self, p: (int, int)):
            return self._heights[self.index_of_point(p)]

        def _not_steep(self, origin: (int, int), target: (int, int)):
            return self.height_of_point(target) <= self.height_of_point(origin) + 1

        def out_neighbors(self, p: (int, int)):
            return [q for q in self._orth_neighbors(p) if self._not_steep(p, q)]

        def in_neighbors(self, p: (int, int)):
            return [q for q in self._orth_neighbors(p) if self._not_steep(q, p)]

        def size(self):
            return self._xsize * self._ysize

    class GraphArray:
        _height_graph: HeightGraph
        _values: list

        def __init__(self, hg, default_value):
            self._height_graph = hg
            self._values = [default_value] * hg.size()

        def __getitem__(self, item):
            return self._values[self._height_graph.index_of_point(item)]

        def __setitem__(self, key, value):
            self._values[self._height_graph.index_of_point(key)] = value

    G = HeightGraph(heights, xsize, ysize)
    visited = GraphArray(G, False)
    distances = GraphArray(G, math.inf)
    Q = Queue()
    if part == 1:
        visited[s] = True
        distances[s] = 0
        Q.enqueue(s)
        while not Q.empty():
            p = Q.dequeue()
            for q in G.out_neighbors(p):
                if not visited[q]:
                    visited[q] = True
                    distances[q] = distances[p] + 1
                    Q.enqueue(q)
        return distances[e]
    else:
        visited[e] = True
        distances[e] = 0
        Q.enqueue(e)
        while not Q.empty():
            p = Q.dequeue()
            for q in G.in_neighbors(p):
                if not visited[q]:
                    visited[q] = True
                    distances[q] = distances[p] + 1
                    Q.enqueue(q)
                    if G.height_of_point(q) == 0:
                        return distances[q]


def day13(part):
    ls = load_input(13) + [""]

    def tokenize(line: str):
        extended_line = line.replace("[", ",[,").replace("]", ",],")
        return [t for t in extended_line.split(",") if t != ""]

    def parse(token_list: list[str]):
        S = Stack()
        L = []
        tokens_list = token_list[1:-1]
        for t in tokens_list:
            if t == "[":
                L_ = []
                L.append(L_)
                S.push(L)
                L = L_
            elif t == "]":
                L = S.pop()
            else:
                L.append(int(t))
        return L

    def compare(obj1: list | int, obj2: list | int) -> int:
        obj1_is_list = isinstance(obj1, list)
        obj2_is_list = isinstance(obj2, list)
        if obj1_is_list and obj2_is_list:
            i = 0
            while i < len(obj1) and i < len(obj2):
                if (x := compare(obj1[i], obj2[i])) != 0:
                    return x
                i += 1
            if i < len(obj1):
                return 1
            elif i < len(obj2):
                return -1
            else:
                return 0
        elif not (obj1_is_list or obj2_is_list):
            return obj1 - obj2
        else:
            if not obj1_is_list:
                obj1 = [obj1]
            else:
                obj2 = [obj2]
            return compare(obj1, obj2)

    def partition(l: list, cmp, low, up, t):
        x = l[t]
        l[t] = l[low]
        i = low
        j = low + 1
        while j <= up:
            if compare(l[j], x) < 0:
                i += 1
                temp = l[i]
                l[i] = l[j]
                l[j] = temp
            j += 1
        l[low] = l[i]
        l[i] = x
        return i

    def quicksort(l: list, cmp, low, up):
        if low >= up:
            return
        i = randint(low, up)
        i = partition(l, cmp, low, up, i)
        quicksort(l, cmp, low, i - 1)
        quicksort(l, cmp, i + 1, up)

    if part == 1:
        i = 1
        s = 0
        for l1, l2, _ in partition_list(ls, 3):
            if compare(parse(tokenize(l1)), parse(tokenize(l2))) <= 0:
                s += i
            i += 1
        return s
    else:
        div1 = [[2]]
        div2 = [[6]]
        lists = [parse(tokenize(l)) for l in ls if l != ""] + [div1, div2]
        quicksort(lists, compare, 0, len(lists) - 1)
        pos1 = lists.index(div1)
        pos2 = lists.index(div2)
        return (pos1 + 1) * (pos2 + 1)


def day14(part):
    ls = load_input(14)

    def parse_line(l):
        points = []
        for p in l.split(" -> "):
            points.append([int(x) for x in p.split(",")])
        return points

    def dimensions(points):
        xmin = 500
        xmax = 500
        ymin = 0
        ymax = 0
        for x, y in points:
            xmin = min(xmin, x)
            xmax = max(xmax, x)
            ymin = min(ymin, y)
            ymax = max(ymax, y)
        return xmin, xmax, ymin, ymax

    def points_between(p1, p2):
        x1, y1 = p1
        x2, y2 = p2
        if x1 > x2:
            points = [(x, y1) for x in range(x2, x1 + 1)]
        elif x1 < x2:
            points = [(x, y1) for x in range(x1, x2 + 1)]
        elif y1 < y2:
            points = [(x1, y) for y in range(y1, y2 + 1)]
        elif y2 < y1:
            points = [(x1, y) for y in range(y2, y1 + 1)]
        else:
            points = [(x1, y1)]
        return points

    class Cave:
        _xmin: int
        _ymin: int
        _xsize: int
        _ysize: int
        _space: list[int]
        _drop_stack: Stack

        def __init__(self, rock_paths):
            self._xmin, xmax, self._ymin, ymax = dimensions(flatten1(rock_paths))
            self._xsize = xmax - self._xmin + 1
            self._ysize = ymax - self._ymin + 1
            self._space = [0] * (self._xsize * self._ysize)
            self._drop_stack = Stack()
            self._last_drop_location = None
            for rp in rock_paths:
                self._build_rock_path(rp)

        def __getitem__(self, item):
            x, y = item
            return self._space[x - self._xmin + self._xsize * (y - self._ymin)]

        def __setitem__(self, key, value):
            x, y = key
            self._space[x - self._xmin + self._xsize * (y - self._ymin)] = value

        def _in_bounds(self, p):
            return self._xmin <= p[0] < self._xmin + self._xsize and self._ymin <= p[1] < self._ymin + self._ysize

        def _drop_one_step(self, p):
            x, y = p
            if (not self._in_bounds((x, y + 1))) or self[(x, y + 1)] == 0:
                return x, y + 1
            elif (not self._in_bounds((x - 1, y + 1))) or self[(x - 1, y + 1)] == 0:
                return x - 1, y + 1
            elif (not self._in_bounds((x + 1, y + 1))) or self[(x + 1, y + 1)] == 0:
                return x + 1, y + 1
            else:
                return p

        def _build_rock_path(self, rock_path):
            for p1, p2 in consecutive_pairs(rock_path):
                for p in points_between(p1, p2):
                    self[p] = 1

        def drop_sand(self, p):
            if p != self._last_drop_location:
                self._drop_stack = Stack()
                self._last_drop_location = p
            if not self._drop_stack.empty():
                p = self._drop_stack.pop()
            if self[p] != 0:
                print("Not enough Room to spawn sand")
                return False
            new_p = p
            old_p = p
            while self._in_bounds(old_p):
                new_p = self._drop_one_step(old_p)
                if new_p == old_p:
                    self[new_p] = 2
                    return True
                self._drop_stack.push(old_p)
                old_p = new_p
            print("Sand dropped to infinity")
            return False

    rock_paths = [parse_line(l) for l in ls]
    if part == 2:
        _, _, _, ymax = dimensions(flatten1(rock_paths))
        rock_paths.append([(500 - ymax - 2, ymax + 2), (500 + ymax + 2, ymax + 2)])
    cave = Cave(rock_paths)
    i = 0
    while cave.drop_sand((500, 0)):
        i += 1
    return i


def day15(part):
    ls = load_input(15)

    @on_list
    def parse_line(l):
        a, b, c, d = multisplit(l, "Sensor at x=", ", y=", ": closest beacon is at x=")
        return (int(a), int(b)), (int(c), int(d))

    def dist(p1, p2):
        return abs(p1[0] - p2[0]) + abs(p1[1] - p2[1])

    def blocked_interval(sensor_pos, beacon_pos, line):
        sensor_x, sensor_y = sensor_pos
        reach = dist(sensor_pos, beacon_pos) - abs(sensor_y - line)
        if reach < 0:
            return None
        else:
            return sensor_x - reach, sensor_x + reach

    class LineIntervals:
        _intervals: list[(int, int)]

        def __init__(self):
            self._intervals = []

        def union_with_interval(self, interval):
            start, end = interval
            s_rank = rank([i[0] for i in self._intervals], start)
            e_rank = rank([i[0] for i in self._intervals], end)
            # start lies between the left of interval s_rank and the left of interval s_rank+1; analogously for end
            if e_rank == -1:
                self._intervals = [interval] + self._intervals
            elif s_rank == -1:
                new_interval = (start, max(end, self._intervals[e_rank][1]))
                intervals_after_new_index = e_rank + 1
                self._intervals = [new_interval] + self._intervals[intervals_after_new_index:]
            else:
                s_contained = start <= self._intervals[s_rank][1]
                e_contained = end <= self._intervals[e_rank][1]
                new_left = self._intervals[s_rank][0] if s_contained else start
                new_right = self._intervals[e_rank][1] if e_contained else end
                new_interval = (new_left, new_right)
                intervals_before_new_index = s_rank - 1 if s_contained else s_rank
                intervals_after_new_index = e_rank + 1
                self._intervals = self._intervals[:intervals_before_new_index + 1] + [new_interval] + self._intervals[
                                                                                                      intervals_after_new_index:]

        def contains_interval(self, interval):
            start, end = interval
            s_rank = rank([i[0] for i in self._intervals], start)
            e_rank = rank([i[0] for i in self._intervals], end)
            if s_rank != e_rank or s_rank == -1:
                return False
            else:
                return end <= self._intervals[e_rank][1]

        def length(self):
            return sum([y - x + 1 for x, y in self._intervals])

        def remove_point(self, x):
            x_rank = rank([i[0] for i in self._intervals], x)
            l, r = self._intervals[x_rank]
            x_contained = x <= r
            if not x_contained:
                return
            if x == l and x == r:
                self._intervals.pop(x_rank)
            elif x == l:
                self._intervals[x_rank] = (l + 1, r)
            elif x == r:
                self._intervals[x_rank] = (l, r - 1)
            else:
                self._intervals = self._intervals[:x_rank] + [(l, x - 1), (x + 1, r)] + self._intervals[x_rank + 1:]

    sensor_beacon_readings = parse_line(ls)
    if part == 1:
        li = LineIntervals()
        for sensor_pos, beacon_pos in sensor_beacon_readings:
            interval = blocked_interval(sensor_pos, beacon_pos, 2000000)
            if interval is not None:
                li.union_with_interval(interval)
            if beacon_pos[1] == 2000000:
                li.remove_point(beacon_pos[0])
        return li.length()
    else:
        # If the unique free spot isn't on the boundary of the [0,4000000]^2 square,
        # then it lies just outside of at least four blocked areas

        # Handle the four boundary lines
        def construct_line(line_number, transposed):
            li = LineIntervals()
            for sensor_pos, beacon_pos in sensor_beacon_readings:
                if transposed:
                    sensor_pos = (sensor_pos[1], sensor_pos[0])
                    beacon_pos = (beacon_pos[1], beacon_pos[0])
                interval = blocked_interval(sensor_pos, beacon_pos, line_number)
                if interval is not None:
                    li.union_with_interval(interval)
            return li

        if not construct_line(0, False).contains_interval((0, 4000000)) \
                or not construct_line(4000000, False).contains_interval((0, 4000000)) \
                or not construct_line(0, True).contains_interval((0, 4000000)) \
                or not construct_line(4000000, True).contains_interval((0, 4000000)):
            return "Free spot on the boundary"

        # The unique free spot lies just outside of at least four blocked areas.
        # If the reading of sensor_pos1 blocks the space to the NW of the free spot,
        # the reading of sensor_pos2 blocks the space to the NE of the free spot,
        # the reading of sensor_pos3 blocks the space to the SE of the free spot,
        # then the free spot is contained in the calculated list of candidates by the following function:
        def candidates(sensor_pos1, beacon_pos1, sensor_pos2, beacon_pos2, sensor_pos3, beacon_pos3):
            x1, y1 = sensor_pos1
            x2, y2 = sensor_pos2
            x3, y3 = sensor_pos3
            if not (x1 < x2 and x1 < x3 and y3 > y1 and y3 > y2):
                return []
            d3 = dist(sensor_pos3, beacon_pos3)
            d2 = dist(sensor_pos2, beacon_pos2)
            d1 = dist(sensor_pos1, beacon_pos1)
            if not (dist(sensor_pos1, sensor_pos2) <= d1 + d2 + 2 and dist(sensor_pos3, sensor_pos2) <= d3 + d2 + 2):
                return []
            if not d1 + d3 + 2 <= dist(sensor_pos1, sensor_pos3) <= d1 + d3 + 4:
                return []
            tx1 = x2 + x3 + y3 - y2 - d2 - d3 - 2
            tx2 = tx1 - 1
            tx3 = tx1 - 2
            ty1 = d1 + d2 + 2 - x2 + x1 + y1 + y2
            ty2 = ty1 + 1
            ty3 = ty1 + 2
            if tx1 % 2 == 0:
                xs = [tx1 / 2, tx3 / 2]
            else:
                xs = [tx2 / 2]
            if ty1 % 2 == 0:
                ys = [ty1 / 2, ty3 / 2]
            else:
                ys = [ty2 / 2]
            return [(x, y) for x in xs for y in ys]

        cand = []
        for s1, b1 in sensor_beacon_readings:
            for s2, b2 in sensor_beacon_readings:
                for s3, b3 in sensor_beacon_readings:
                    cand.extend(candidates(s1, b1, s2, b2, s3, b3))

        def free_and_in_rect(point):
            for s, b in sensor_beacon_readings:
                if dist(s, point) <= dist(s, b):
                    return False
            return 0 <= point[0] <= 4000000 and 0 <= point[1] <= 4000000

        for c in cand:
            if free_and_in_rect(c):
                return c[0] * 4000000 + c[1]


def day16(part):  # works, but could be faster - ~15mio recursive calls on part 2.
    ls = load_input(16)

    @on_list
    def parse_line(l):
        a, b, *c = multisplit(l, "Valve ", " has flow rate=", "; tunnels lead to valves ", "; tunnel leads to valve ",
                              ", ")
        return a, int(b), c

    data = parse_line(ls)
    nodes = [x[0] for x in data]
    dist = dict()
    for v in nodes:
        for w in nodes:
            dist[(v, w)] = math.inf
        dist[(v, v)] = 0
    for v, _, neighbors in data:
        for w in neighbors:
            dist[(v, w)] = 1
    for w in nodes:
        for u in nodes:
            for v in nodes:
                x = dist[(u, w)] + dist[(w, v)]
                if x < dist[(u, v)]:
                    dist[(u, v)] = x
    relevant_nodes = ["AA"] + [v for v, d, _ in data if d != 0 and v != "AA"]
    dist_matrix = [[dist[(v, w)] for w in relevant_nodes] for v in relevant_nodes]
    flow_vector = [0] + [d for v, d, _ in data if d != 0 and v != "AA"]
    max_flow = max(flow_vector)
    min_dist = min([min([i for i in x if i != 0]) for x in dist_matrix])
    num_nodes = len(relevant_nodes)

    class Opt:
        def __init__(self):
            self.value = 0

    o = Opt()
    if part == 1:
        def search_for_best(rem_time, current_node, opened_valves, value):
            t = int(rem_time / (min_dist + 1))
            if (t * (rem_time - t * (min_dist + 1)) + (min_dist + 1) * (t - 1) * t / 2) * max_flow <= o.value - value:
                return value
            possible_next_nodes = [(i, dist_matrix[current_node][i], flow_vector[i]) for i in range(num_nodes) if
                                   dist_matrix[current_node][i] + 2 <= rem_time and not (opened_valves >> i) & 1]
            l = len(possible_next_nodes)
            if l == 0:
                o.value = max(o.value, value)
                return value
            if l == 1:
                x = value + (rem_time - possible_next_nodes[0][1] - 1) * possible_next_nodes[0][2]
                o.value = max(o.value, x)
                return x
            opt = value
            for node, d, f in possible_next_nodes:
                opt = max(opt, search_for_best(rem_time - d - 1, node, opened_valves + 2 ** node,
                                               value + (rem_time - d - 1) * f))
            return opt

        return search_for_best(30, 0, 0, 0)
    else:
        def search_for_best(rem_time_self, rem_time_ele, cn_self, cn_ele, opened_valves, value):
            if rem_time_self >= rem_time_ele:
                t = int((rem_time_self + rem_time_ele) / (min_dist + 1))
                if (t * ((rem_time_self + rem_time_ele) - t * (min_dist + 1)) + (min_dist + 1) * (
                        t - 1) * t / 2) * max_flow <= o.value - value:
                    return value
                possible_next_nodes = [(i, dist_matrix[cn_self][i], flow_vector[i]) for i in range(num_nodes) if
                                       dist_matrix[cn_self][i] + 2 <= rem_time_self and not (opened_valves >> i) & 1]
                l = len(possible_next_nodes)
                if l == 0:
                    if rem_time_ele != 0:
                        value = search_for_best(0, rem_time_ele, cn_self, cn_ele, opened_valves, value)
                    o.value = max(o.value, value)
                    return value
                opt = value
                for node, d, f in possible_next_nodes:
                    opt = max(opt, search_for_best(rem_time_self - d - 1, rem_time_ele, node, cn_ele,
                                                   opened_valves + 2 ** node, value + (rem_time_self - d - 1) * f))
                return opt
            else:
                t = int((rem_time_self + rem_time_ele) / (min_dist + 1))
                if (t * ((rem_time_self + rem_time_ele) - t * (min_dist + 1)) + (min_dist + 1) * (
                        t - 1) * t / 2) * max_flow <= o.value - value:
                    return value
                possible_next_nodes = [(i, dist_matrix[cn_ele][i], flow_vector[i]) for i in range(num_nodes) if
                                       dist_matrix[cn_ele][i] + 2 <= rem_time_ele and not (opened_valves >> i) & 1]
                l = len(possible_next_nodes)
                if l == 0:
                    if rem_time_self != 0:
                        value = search_for_best(rem_time_self, 0, cn_self, cn_ele, opened_valves, value)
                    o.value = max(o.value, value)
                    return value
                opt = value
                for node, d, f in possible_next_nodes:
                    opt = max(opt, search_for_best(rem_time_self, rem_time_ele - d - 1, cn_self, node,
                                                   opened_valves + 2 ** node, value + (rem_time_ele - d - 1) * f))
                return opt

        return search_for_best(26, 26, 0, 0, 0, 0)


if __name__ == '__main__':
    # input for day i has to be in file "input_i"
    print(day16(1))
